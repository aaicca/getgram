import Vue from 'vue'
import VueI18n from 'vue-i18n';
import en from "./local/en/";
import es from "./local/es/";

Vue.use(VueI18n)

function loadLocaleMessages(){

  const messages = {
    en,
    es
  }

  return messages;
}

function detectLanguage(){
  let language = "en";
  if(navigator.language.split('-')[0] == "es"){
    language = "es";
  }
  return language;
}

export default new VueI18n({
  locale: detectLanguage(),
  fallbackLocale: 'en',
  messages: loadLocaleMessages()
})
