import Vue from 'vue'
import App from './App.vue'
import router from './router'
import BootstrapVue from "bootstrap-vue";
import VueIziToast from 'vue-izitoast';
import i18n from './i18n';
import VueParticles from 'vue-particles';
import axios from 'axios'
import VueAxios from 'vue-axios'

import 'izitoast/dist/css/iziToast.min.css';
import "bootstrap/dist/css/bootstrap.css"
import "bootstrap-vue/dist/bootstrap-vue.css"
import "bootstrap-dark/src/bootstrap-dark.css";
 
Vue.use(VueAxios, axios)
Vue.use(VueParticles);
Vue.use(BootstrapVue);
Vue.use(VueIziToast, {
  progressBar: false,
  closeOnClick: true,
  icon: false
});

Vue.config.productionTip = false;

new Vue({
  router,
  i18n,
  render: h => h(App)
}).$mount('#app')
