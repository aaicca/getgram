import firebase from "firebase";

const firebaseConfig = {
  apiKey: "AIzaSyDJYhwItCgTVHEmNYOiC9L3SvfcSBivkU4",
  authDomain: "getgram-1e52f.firebaseapp.com",
  databaseURL: "https://getgram-1e52f.firebaseio.com",
  projectId: "getgram-1e52f",
  storageBucket: "getgram-1e52f.appspot.com",
  messagingSenderId: "772477580192",
  appId: "1:772477580192:web:48094039e788e05b5ceac5",
  measurementId: "G-N49MNCV2NL"
};
  
firebase.initializeApp(firebaseConfig);

const auth = firebase.auth();

export {
    auth
}